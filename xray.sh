#!/bin/sh
if [ ! -f UUID ]; then
  UUID="ff3f5af8-a39e-11ed-ad82-cf1c51ea846b"
fi

# Set config.json
sed -i "s/PORT/$PORT/g" /etc/xray/config.json
sed -i "s/UUID/$UUID/g" /etc/xray/config.json

echo starting xray platform
echo starting with UUID:$UUID
echo listening at 0.0.0.0:$PORT

exec "$@"

